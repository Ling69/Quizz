# Projet Quizz

## Maquette de mon quizz
![wireframe quizz](image/2048.png)

## Fonctionnalités de mon quizz
    1. Ce projet est realisé avec Html,CSS et JavaScript
    2. Choisir le nombre de questions
    3. Affichage des questions et choix de la réponse 
    4. Affichage du score
    5. Page responsive 

## Compétences JS
    Utilise: variables, conditions, tableaux, boucles et fonction
    DOM: querySelector/Events, addEventListener


## Le lien de Gitlab: 
https://ling69.gitlab.io/Quizz


