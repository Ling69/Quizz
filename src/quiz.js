
const myQuestions = [
  {
    question: "Comment afficher Bonjour sur la console?",
    answers: {
      a: "log('Bonjour')",
      b: "prompt('Bonjour')",
      c: "console.log('Bonjour')",
      d: "console('Bonjour')"
    },
    correctAnswer: "c"
  },
  {
    question: "Comment trier un tableau de nombres T1 dans l'ordre décroissant ?",
    answers: {
      a: "T1.sort(function(a,b){retrun b-a});",
      b: "T1.rsort()",
      c: "T1.sort(-1)",
      d: "T1.sort('Desc')"
    },
    correctAnswer: "a"
  },
  {
    question: "Que signifie NaN?",
    answers: {
      a: "Not a Number",
      b: "Not area Negative",
      c: "Not at NULL",
      d: "Number area NULL"
    },
    correctAnswer: "a"
  },
  {
    question: "Qu'est ce que JSON par rapport au JavaScript?",
    answers: {
      a: "une variable d'environnement système",
      b: "un langage de requêtes",
      c: "un format d'échange de données texte",
      d: "un langage de programmation dérivé"
    },
    correctAnswer: "c"
  },
  {
    question: "Quel opérateur est utilisé pour concaténer des chaînes de caractères?",
    answers: {
      a: "&",
      b: ".",
      c: "add()",
      d: "+"
    },
    correctAnswer: "d"
  },
  {
    question: "Pour tester de nombreuses conditions sur la même variable on utilise ",
    answers: {
      a: "for()",
      b: "switch()",
      c: "if()",
      d: "while()"
    },
    correctAnswer: "b"
  },
  {
    question: "A quoi sert l'opérateur === ?",
    answers: {
      a: "à comparer le type et la valeur de 2 données",
      b: "à opérer une affectation après la comparaison",
      c: "c'est un comparateur logique",
      d: "n'existe pas en JavaScript",
    },
    correctAnswer: "a"
  },
  {
    question: "Math.ceil(3.6) renvoie",
    answers: {
      a: "n'existe pas en JavaScript",
      b: "3 4 5 6",
      c: "4",
      d: "3",
    },
    correctAnswer: "c"
  },
  {
    question: "Que retourne typeof (1>2) ?",
    answers: {
      a: "true",
      b: "integer",
      c: "boolean",
      d: "false",
    },
    correctAnswer: "c"
  },
  {
    question: "De quelle façon déclare-t-on un tableau T1 de 20 éléments ?",
    answers: {
      a: "var T1(20)",
      b: "T1=new Array(20)",
      c: "var T1[20]",
      d: "var T1=array(20)",
    },
    correctAnswer: "b"
  },
  {
    question: "Comment lire le jour du mois dans une date ?",
    answers: {
      a: "Day()",
      b: "Date.parse()",
      c: "getDate()",
      d: "getDay()",
    },
    correctAnswer: "c"
  },
  {
    question: "Quelle fonction permet de temporiser l'exécution d'une commande ?",
    answers: {
      a: "wait()",
      b: "SetTimer()",
      c: "sleep()",
      d: "setTimeout()",
    },
    correctAnswer: "d"
  },
  {
    question: "Quelle propriété permet d'identifier le n° de version du navigateur ?",
    answers: {
      a: "navigator.appVersion",
      b: "navigator.userAgent",
      c: "navigator.version",
      d: "navigator.platform",
    },
    correctAnswer: "a"
  },
  {
    question: "Quelle fonction est l'inverse de split() ?",
    answers: {
      a: "join()",
      b: "concat()",
      c: "append()",
      d: "unite()",
    },
    correctAnswer: "a"
  },
  {
    question: "A quoi sert l'opérateur #= ?",
    answers: {
      a: "à comparer 2 pointeurs",
      b: "c'est un comparateur logique",
      c: "à comparer le type et la valeur de 2 données",
      d: "n'existe pas en JavaScript",
    },
    correctAnswer: "d"
  },
  {
    question: "Quelle propriété permet d'identifier l'OS de l'utilisateur ?",
    answers: {
      a: "navigator.userAgent",
      b: "navigator.system",
      c: "navigator.platform",
      d: "navigator.os",
    },
    correctAnswer: "c"
  },
  {
    question: "La fonction javascript_info()",
    answers: {
      a: "permet de déboguer une variable",
      b: "permet de connaître l'OS de l'utilisateur",
      c: "n'existe pas",
      d: "renvoie la version de JavaScript utilisée",
    },
    correctAnswer: "c"
  },
];


let answerEls = document.querySelectorAll(".answer"); //this is radio button definition
const quiz = document.querySelector('#quiz');
const question = document.querySelector('p');
const answera = document.querySelector('#answera');
const answerb = document.querySelector('#answerb');
const answerc = document.querySelector('#answerc');
const answerd = document.querySelector('#answerd');
const validerBtn = document.querySelector('#valider');
const lance = document.querySelector('.lance')
const debut = document.querySelector('#debut')
const deuxieme = document.querySelector('#deuxieme')
let choice = document.querySelector('select')
let selectedA = 0;
const scoreHTML = document.querySelector('.score')
let compt = document.querySelector('#timer')
let test;
let id = 0;
let i = 10;

lance.addEventListener('click', () => {
  //pour cacher la première page
  quiz.style.display = 'block';
  debut.style.display = 'none';
  deuxieme.style.display = 'none';
  selectedA = choice.value;

  test = setInterval(function () {
    compt.textContent = i--;
    if (i < 0) {
      currentQuestion++;
      loadQuiz();
    }
  }, 1000);
});


let currentQuestion = 0;
let score = 0;

loadQuiz();

function loadQuiz() {

  i = 10;
  //id est la question de tableau, quand id = 0, on prend toutes les questions.
  if (id < myQuestions.length - 1) {
    id++;

    //pour décocher le choix précedent 
    deselectAnswers();

    //pour afficher chaque page
    const currentQuestionData = myQuestions[currentQuestion];
    question.textContent = currentQuestionData.question;
    answera.textContent = currentQuestionData.answers.a;
    answerb.textContent = currentQuestionData.answers.b;
    answerc.textContent = currentQuestionData.answers.c;
    answerd.textContent = currentQuestionData.answers.d;
  }
}

function getSelected() {

  let answer = undefined;

  answerEls.forEach((answerEl) => {
    //console.log(answer.checked);
    if (answerEl.checked) {
      answer = answerEl.id;
    }
  });
  return answer;
}


function deselectAnswers() {
  answerEls.forEach((answerEl) => {
    answerEl.checked = false;
  });
}

validerBtn.addEventListener("click", () => {

  const answer = getSelected();

  if (answer) {
    //console.log(answer);
    if (answer === myQuestions[currentQuestion].correctAnswer) {
      score++;
      //ajouter score 
      scoreHTML.textContent = 'score :' + score;
    }
    currentQuestion++;

    // pour choisir le nombre de questions à poser
    if (currentQuestion < selectedA) {

      return loadQuiz();
    } else {

      quiz.innerHTML =
        `<div class="center"> 
            <h2>You score is ${score}/${selectedA} !</h2>
            <button onclick="location.reload()">Reload</button></div>`;

    }
  }
});

